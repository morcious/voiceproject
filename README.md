Introduction
Personal Digital Assistant applications and devices have been here for a long time with enhancements in technology and user interfaces. Interfaces such as keyboard, mouse, touch screens are some common form of interfaces. The modern era sees voice as an input while a voice recognizing device or software can provide voice interface to accept voice commands from the users. Some examples of such are Ok Google facility in android based smartphones, Cortana, Alexa, picroft (MyCroft), Siri, Bixby etc. All these provide voice enhanced voice assistance and support automations. Developing into such type of assistant can provide users with a lot of productive outcomes.

This report is about a framework designed so that anyone can add commands of their choice and make proper configurations as required to fully customize the features according to their needs. The base code developed in this project is based on a very popular design pattern called command design pattern. Such pattern suits into places where we use a command based applications, mostly games are popular for this however this project make uses of an infinite loop for always listening to the voice command. This report will see how the different codes have been put together and provides information for those who want to improve, explore or develop their own applications.

Program Flow and Flow Chart
The program basically starts with an initialize file that identifies all the commands in a command folder while registering each of these commands to a word or a phrase as mentioned in a configuration file. The configuration file helps to map the commands to the relevant word or phrase. Once the commands have been registered, the importing of required audio and voice related modules are initialized and configured. Then the system will look out for the keyword that is mentioned in another configuration file. This keyword is the call-up word. One can use anything here, for example Alexa; if one is trying to make an Alexa like assistant. When the call-up word is called, the system should signal for waiting for command, this will be based on the device that one is developing for. As an example of this report, a sound has been used to indicate that the assistant is ready for a command. The system then invokes the command pattern concept to execute the command that has been given. Once the command task is done, the assistant returns back to the previous state of listening up call-up word and it goes on for the program's lifetime.
 


Command Pattern
A command pattern generally includes an abstract command class with an execute function while the execute function is implemented in the relevant command. These commands are generally registered into a command repository. Here the invoker is a class that acts as the registrar for the commands and the invoker of the execute function. In command pattern, usually the output interface is passed as arguments to the command's constructors. This will help the command generate output at the desired interface. One can extend the number of output interfaces (here only the Receiver class is the output interface) as required.
The command handler class or the CommandManager class helps to initialize the commands by properly handling the listening and handling functions. The command handlers registers the commands in the Invoker command repository. 
 Implementation
By default the main.pyw file is our executable file. We generally do not have to edit the file unless adding up custom features on it. Basically open up the main.pyw file and running it would start the voice assistant. To add own commands there are basic steps. The following is the sample for implementing a command, an example of CalculateCommand implemented in the given framework.
from abc import ABCMeta, abstractmethod
from engine.commands.commands import ICommand
class CalculateCommand(ICommand):  # pylint: disable=too-few-public-methods
    """A Command object, that implements the ICommand interface and
    runs the command on the designated receiver"""
    def __init__(self, receiver):
        self._receiver = receiver
    def execute(self,parameters):
        checkFor=parameters.split()[0]
        if(checkFor=="for"):
            parameters=parameters.replace(checkFor,'')
        self._receiver.speak('Caclulating '+parameters)
        self._receiver.searchFor("="+parameters) 
The highlighted functions for initialization and execution are the main parts that need to be coded. One has to extend the base class ICommand and then override the function execute from the abstract class. The command takes in the parameters. The parameters are those that follow after the command has been spoken, for example, the voice command "calculate 2 into 2" would call the command  CalculateCommand and the command handler will reduce the command by replacing the calculate keyword and the remaining '2 into 2' would be provided as arguments.
Once the class has been written, it has to be written in the config.txt file, mentioning the variable name, class name and the voice keyword, for example the following is the current contents of the config.txt file.
what,WhatCommand,what
play,PlayCommand,play
fail,FailedCommand,fail
search,SearchCommand,search
recepie,RecepieCommand,recepie
howareyou,HowAreYou,howAreYou
calculate,CalculateCommand,calculate
pdf,SearchPdfCommand,pdf
convert,ConvertCommand,convert
whatisyourname,WhatIsYourName,whatIsYourName
who,WhoCommand,who
Here the file has comma separated values, first is the variable name, second is the Command and the third is the  voice word that will be mapped to the command. One word commands usually have arguments (parameters) while there can be some commands that are actually phrases, for example in the implemented system the command HowAreYou could be executed without parameters. In the system, a phrase is checked first if such case exists. One Important thing to note while implementing such command is that the voice keyword must be in camel case; as for example, HowAreYou Command is identified as howAreYou.
Once the configuration has been written, running the main function (main.pyw) will automatically include the command. The following points summarize the whole concept of developing using this framework. Developing using the framework would include the activities that are given below.
Steps of developing Voice Assistant using the voice framework.
1.	Extend ICommand to a Special Command as required.
2.	Override the execute function.
3.	Add the information of the new command to config.txt file.
4.	Modify the json file 'device' for appropriate callup keyword, currently cuckoo and pipi
5.	Run the system to enjoy the new command.
Conclusion Recommendations
Developing using the system is not limited to just extending on the commands, One can even modify many codes to give more flexibility. It is not limited to items that are just in the given framework. One can use this framework to write upon to different embedded systems, own desktop computers, laptops or even smart phones. Sometimes it is just helpful that we do not require to spend more on high technology commercial devices even just of simple and limited tasks. There are many possibilities that we can end up with but each end will help us start a new journey.

