'''
Project: Open and Flexible Voice Project Framework
Author: Subein Byanjankar, Msc ITM, Analyst Programmer, Nepal
Project-Title: PersonalDesktopAssistant
FileName: initialize.py
'''
'''
The following set of code helps to import all the files inside the commands folder automatically, one can set the commands inside the commands folder
and then it is automatically imported here.
'''
import json
import joblib
from pathlib import Path
f = open('device')
data = json.load(f)
callups=data['callup']

    
import glob
files=glob.glob('engine/commands/*.py',recursive=False)
for file in files:
    file=file.replace('\\','.')
    file=file.replace('/','.')
    file=file.replace('.py','')
    exec("from "+file+" import *")

'''
    Importing the Invoker and the Receiver, Invoker is basically the repository of commands while Receiver is the output driver
'''
from engine.interface.Invoker import Invoker
from engine.interface.Receiver import Receiver

'''
    This is a the speech related part where we are using speech_recognition, py audio and gtts (google text to speech engine)
'''
import speech_recognition as sr
import pyaudio
import gtts

'''this is for machine learning
'''
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline


# The Importer Class, CommandManager or Command Handler is a class that helps to handle the commands related task
class CommandManager:
    "The CommandManager Class"

    def __init__(self):
        self.RECEIVER = Receiver()
        self.INVOKER = Invoker()
        self.listener1 = sr.Recognizer()
        self.listener2 = sr.Recognizer()
        'self.registerCommands()'
        self.fileRegisterCommands()
        self.registerAICommands()

    def fileRegisterCommands(self):
        with open("config",'r') as f:
            lines=f.readlines()
        for i in lines:
            i=i.replace('\n','')
            arr=i.split(",")
            arr[0] = arr[1]
            klass = globals()[arr[1]]
            self.INVOKER.register(arr[2], klass(self.RECEIVER))

    def showCommands(self):
        self.INVOKER.showCommands()
    	
    def handleCommand(self,command):
        mainCommand=self.handlePhraseCommand(command)
        if mainCommand==False:
            mainCommand=command.split()[0]
        '''command=command.replace(mainCommand,'')'''
        self.INVOKER.execute(mainCommand,command)
        ''' command variable here is the parameter passed to the main command'''
    
    def handleCommandAI(self,command):
        '''in this part
            we must handle the machine learning, at this moment we have handled the 
            command to check the phrases or the first word as command, the commands 
            may be phrased commands that will be a whole phrase command itself,
            rather than this we would further 
        '''
        user_message = command;
        mainCommand = self.model.predict([user_message])

        #check if the main command predicted is within the command list,
        theCommands=self.INVOKER.getCommands();
        if mainCommand[0] in theCommands.keys():
            print (mainCommand[0])
            self.INVOKER.execute(mainCommand[0],command)
        else:
            self.handleCommand(command)


        '''
        mainCommand=self.handlePhraseCommand(command)
        if mainCommand==False:
            mainCommand=command.split()[0]
        '''
        '''command=command.replace(mainCommand,'')'''
        
        ''' command variable here is the parameter passed to the main command'''

    def handlePhraseCommand(self,command):
        return self.INVOKER.checkPhraseCommand(command)

    def listening(self):
        try:
            #self.listener1.lang="ne-NP"
            with sr.Microphone() as source:
                self.listener1.adjust_for_ambient_noise(source, duration = 2)
                print("Listening call")
                'Herer write a code that will help a green signal led to be lighted'
                voice=self.listener1.listen(source)
                command=self.listener1.recognize_google(voice)
                command=command.lower()
                print(command)
                res = [ele for ele in callups if(ele in command)]
                if bool(res)==True:
                    '''command=command.replace('cuckoo','')'''
                    if command=='':
                        return False
                    return command
                else:
                    return True
        except Exception as e:
            #pass if error occors
            return True


    def listenCommand(self):
        try:
            with sr.Microphone() as source:
                self.listener2.adjust_for_ambient_noise(source, duration = 2)
                print("Listening Command")
                self.RECEIVER.playSound("media/cukooheard.wav")
                voice=self.listener2.listen(source)
                self.RECEIVER.playSound("media/cukooprocess.wav")
                command=self.listener2.recognize_google(voice)
             
                command=command.lower()
                print(command)
                if command=='':
                    return False
                return command
        except Exception as e:
            #pass if error occors
            return True

    def registerAICommands(self,filename='',model=''):
        p=Path('./'+model);
        if model!='':
            if p.exists():
                self.load_model(model)
            elif filename !='':
                self.prepareAndLoadModel(filename,model)
        elif filename !='':
            if model!='':
                self.prepareAndLoadModel(filename,model)
            else:
                self.prepareAndLoadModel(filename)
        else:
            self.prepareAndLoadModel()

    def prepareAndLoadModel(self,filename='training.json',model='model.model'):
        # Load the training data from the JSON file
        with open(filename, 'r') as file:
            training_data = json.load(file)
        # Extract intents and messages from the training data
        n=3
        intents = [item['name'] for item in training_data['intents']]
        #for equally dividing the labels with each variation
        intents = [intent for intent in intents for _ in range(n)]


        messages = [example for intent in training_data['intents'] for example in intent['examples']]
        
        # Create a machine learning pipeline for text classification
        nmodel = Pipeline([
            ('vectorizer', CountVectorizer()),  # Convert text data to numerical features
            ('classifier', MultinomialNB()),   # Multinomial Naive Bayes classifier
        ])
        # Fit the model on the training data
        X=messages
        #X=CountVectorizer().fit_transform(messages)
        
        nmodel.fit(X,intents)
        # Save the trained model to a file
        joblib.dump(nmodel, model)

        self.load_model(model);

    def load_model(self, filename='model.model'):
        # Load the model from the file
        loaded_model = joblib.load(filename)

        # Assign the loaded model to self.model
        self.model = loaded_model

    def testMessage(self,msg):
        command=msg
        command=command.lower()
        print(command)
        return command
    