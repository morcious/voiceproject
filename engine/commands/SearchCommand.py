'''
Project: Open and Flexible Voice Project
Author: Subein Byanjankar, Msc ITM, Analyst Programmer, Nepal
Project-Title: PersonalDesktopAssistant
FileName: SearchCommand.py
'''

from abc import ABCMeta, abstractmethod
from engine.commands.commands import ICommand


class SearchCommand(ICommand):  # pylint: disable=too-few-public-methods
    """A Command object, that implements the ICommand interface and
    runs the command on the designated receiver"""

    def __init__(self, receiver):
        self._receiver = receiver

    def execute(self,parameters):
        checkFor="search";
        parameters=parameters.replace(checkFor,'',1)
        checkFor="for";
        parameters=parameters.replace(checkFor,'',1)
        self._receiver.speak('Searching for '+parameters)
        self._receiver.searchFor(parameters)