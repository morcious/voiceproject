'''
Project: Open and Flexible Voice Project
Author: Subein Byanjankar, Msc ITM, Analyst Programmer, Nepal
Project-Title: PersonalDesktopAssistant
FileName: WhoCommand.py
'''
import re
from abc import ABCMeta, abstractmethod
from engine.commands.commands import ICommand
from engine.processor.LanguageProcessor import LanguageProcessor

import wikipedia as wp
class WhoCommand(ICommand):  # pylint: disable=too-few-public-methods
    """A Command object, that implements the ICommand interface and
    runs the command on the designated receiver"""

    def __init__(self, receiver):
        self._receiver = receiver

    def execute(self,parameters):
        pattern = r"who is (.+)"
        match = re.search(pattern, parameters)
        result="";
        if match:
            parameters = match.group(1)
            pageid=parameters.replace(" ", "_")
            try:
                print("Getting information of ",parameters.replace(" ", "_"))
                print(" from wikipedia. Please wait.")
                result =wp.summary(parameters.replace(" ", "_"),sentences=10)
            except:
                self._receiver.speak("Sorry, I could not find the information.")
        print("Here is the result")
        print(result)
        if result!="":
            self._receiver.speak(result)
