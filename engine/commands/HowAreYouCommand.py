'''
Project: Open and Flexible Voice Project
Author: Subein Byanjankar, Msc ITM, Analyst Programmer, Nepal
Project-Title: PersonalDesktopAssistant
FileName: HowAreYourCommand.py
'''

from abc import ABCMeta, abstractmethod
from engine.commands.commands import ICommand


class HowAreYou(ICommand):  # pylint: disable=too-few-public-methods
    """A Command object, that implements the ICommand interface and
    runs the command on the designated receiver"""

    def __init__(self, receiver):
        self._receiver = receiver

    def execute(self,parameters):
        self._receiver.speak('I am fine thank you, How about you?')
