'''
Project: Open and Flexible Voice Project
Author: Subein Byanjankar, Msc ITM, Analyst Programmer, Nepal
Project-Title: PersonalDesktopAssistant
FileName: WhatCommand.py
'''
import re
from abc import ABCMeta, abstractmethod
from engine.commands.commands import ICommand
from engine.processor.LanguageProcessor import LanguageProcessor
class CapitalCityCommand(ICommand):  # pylint: disable=too-few-public-methods
    """A Command object, that implements the ICommand interface and
    runs the command on the designated receiver"""

    def __init__(self, receiver):
        self._receiver = receiver

    def execute(self,parameters):
        p=LanguageProcessor()
        #first match the pattern and sentence after the what command
        pattern = r"what (.+)"
        match = re.search(pattern, parameters)
        location=""
        if match:
            parameters = match.group(1)
            tokens,filtered_tokens,named_entities=p.processSentence(parameters)
            # Process named entities
            for entity in named_entities:
                print(entity.label())
                if entity.label() == 'GPE':  # GPE: Geo-Political Entity (location)
                    location = ' '.join([word for word, tag in entity.leaves()])
                elif entity.label() == 'NE':
                    location = ' '.join([word for word, tag in entity.leaves()])
        print("Finding the capital city of: ",location)