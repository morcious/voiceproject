'''
Project: Open and Flexible Voice Project
Author: Subein Byanjankar, Msc ITM, Analyst Programmer, Nepal
Project-Title: PersonalDesktopAssistant
FileName: commands.py
'''

"The Command Pattern Concept"
from abc import ABCMeta, abstractmethod


class ICommand(metaclass=ABCMeta):  # pylint: disable=too-few-public-methods
    "The command interface, that all commands will implement"
    @staticmethod
    @abstractmethod
    def execute(self,parameters=''):
        "The required execute method that all command objects will use"
