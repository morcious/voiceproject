'''
Project: Open and Flexible Voice Project
Author: Subein Byanjankar, Msc ITM, Analyst Programmer, Nepal
Project-Title: PersonalDesktopAssistant
FileName: WhoCommand.py
'''
import webbrowser
from abc import ABCMeta, abstractmethod
from engine.commands.commands import ICommand

class ShopCommand(ICommand):  # pylint: disable=too-few-public-methods
    """A Command object, that implements the ICommand interface and
    runs the command on the designated receiver"""

    def __init__(self, receiver):
    	self._receiver = receiver

    def execute(self,parameters):
    	checkFor=parameters.split()[0]
    	if(checkFor=="in"):
            parameters=parameters.replace(checkFor,'')
    	elif(checkFor=="for"):
    		parameters=parameters.replace(checkFor,'')

    	if("daraz" in parameters or "daras" in parameters):
    		if(" in " in parameters):
    			parameters=parameters.replace(" in ","")
    		parameters=parameters.replace("daraz","")
    		parameters=parameters.replace("daras","")
    		webbrowser.open('https://daraz.com.np/catalog?q='+parameters, new=2)

    	elif("amazon" in parameters):
    		if(" in " in parameters):
    			parameters=parameters.replace(" in ","")
    		parameters=parameters.replace("amazon","")
    		webbrowser.open('https://www.amazon.com/s?k='+parameters, new=2)    	