'''
Project: Open and Flexible Voice Project
Author: Subein Byanjankar, Msc ITM, Analyst Programmer, Nepal
Project-Title: PersonalDesktopAssistant
FileName: WhoCommand.py
'''
import python_weather
import asyncio

from abc import ABCMeta, abstractmethod
from engine.commands.commands import ICommand

class WeatherCommand(ICommand):  # pylint: disable=too-few-public-methods
    """A Command object, that implements the ICommand interface and
    runs the command on the designated receiver"""

    def __init__(self, receiver):
    	self.client = python_weather.Client(format=python_weather.IMPERIAL)
    	self._receiver = receiver

    def execute(self,parameters):
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.getweather(parameters))

    async def getweather(self,parameters):
    	weather = await self.client.find("Patan, Lalitpur")
    	self._receiver.speak("The Current temperature is "+str(weather.current.temperature)+"degree Farenheit")
    	print(weather.current.sky_text)
    	self._receiver.speak("The weekly forecast is as follows ")
    	for forecast in weather.forecasts:
            self._receiver.speak("On "+ str(forecast.date).replace("00:00:00","") +" it is "+ forecast.sky_text + ". The temperature is "+ str(forecast.temperature)+" degree Farenheit")
    	await self.client.close()
