'''
Project: Open and Flexible Voice Project
Author: Subein Byanjankar, Msc ITM, Analyst Programmer, Nepal
Project-Title: PersonalDesktopAssistant
FileName: ConvertCommand.py
'''

from abc import ABCMeta, abstractmethod
from engine.commands.commands import ICommand


class ConvertCommand(ICommand):  # pylint: disable=too-few-public-methods
    """A Command object, that implements the ICommand interface and
    runs the command on the designated receiver"""

    def __init__(self, receiver):
        self._receiver = receiver

    def execute(self,parameters):
        checkFor=parameters.split()[0]
        if(checkFor=="for"):
            parameters=parameters.replace(checkFor,'')
        self._receiver.speak('Converting '+parameters)
        self._receiver.searchFor(parameters)
