'''
Project: Open and Flexible Voice Project
Author: Subein Byanjankar, Msc ITM, Analyst Programmer, Nepal
Project-Title: PersonalDesktopAssistant
FileName: WhatCommand.py
'''
import re
from abc import ABCMeta, abstractmethod
from engine.commands.commands import ICommand
from engine.processor.LanguageProcessor import LanguageProcessor

import wikipedia as wp
class WhatCommand(ICommand):  # pylint: disable=too-few-public-methods
    """A Command object, that implements the ICommand interface and
    runs the command on the designated receiver"""

    def __init__(self, receiver):
        self._receiver = receiver

    def execute(self,parameters):
        pattern = r"what is (.+)"
        match = re.search(pattern, parameters)
        result="";
        if match:
            parameters = match.group(1)
            try:
                print("Searching for ",parameters.replace(" ", "_"))
                result =wp.summary(parameters.replace(" ", "_"),sentences=10)
            except:
                self._receiver.speak("Sorry, I could not find the information.")
        print(result)
        if result!="":
            self._receiver.speak(result)