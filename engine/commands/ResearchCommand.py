'''
Project: Open and Flexible Voice Project
Author: Subein Byanjankar, Msc ITM, Analyst Programmer, Nepal
Project-Title: PersonalDesktopAssistant
FileName: WhoCommand.py
'''
import webbrowser
from abc import ABCMeta, abstractmethod
from engine.commands.commands import ICommand

class ResearchCommand(ICommand):  # pylint: disable=too-few-public-methods
    """A Command object, that implements the ICommand interface and
    runs the command on the designated receiver"""

    def __init__(self, receiver):
    	self._receiver = receiver

    def execute(self,parameters):
    	checkFor=parameters.split()[0]
    	if(checkFor=="on"):
            parameters=parameters.replace(checkFor,'')
    	if(checkFor=="about"):
            parameters=parameters.replace(checkFor,'')
    	if(checkFor=="for"):
            parameters=parameters.replace(checkFor,'')
    	webbrowser.open('https://scholar.google.com/scholar?hl=en&q='+parameters, new=2)


