'''
Project: Open and Flexible Voice Project
Author: Subein Byanjankar, Msc ITM, Analyst Programmer, Nepal
Project-Title: PersonalDesktopAssistant
FileName: Receiver.py
'''

import pyaudio
import gtts
import os
import pywhatkit
from playsound import playsound

class Receiver:
    "The Receiver"

    @staticmethod
    def speak(text):
        tts = gtts.gTTS(text, lang="en",tld="com.au",slow=False)				
        testfile = "./tmp/temp.mp3"
        tts.save(testfile)
        os.popen('mpg123 '+testfile).read()
        os.remove(testfile)

    @staticmethod
    def playYoutube(par):
        pywhatkit.playonyt(par)

    @staticmethod
    def searchFor(par):
        pywhatkit.search(par)
        
    @staticmethod
    def infoFor(par):
        try:
            return pywhatkit.info(par,lines=10)
        except:
            return 0
    @staticmethod
    def playSound(par):
        playsound(par)