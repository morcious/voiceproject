'''
Project: Open and Flexible Voice Project
Author: Subein Byanjankar, Msc ITM, Analyst Programmer, Nepal
Project-Title: PersonalDesktopAssistant
FileName: Invoker.py
'''

from re import sub
class Invoker:
    "The Invoker Class"

    def __init__(self):
        self._commands = {}

    def register(self, command_name, command):
        "Register commands in the Invoker"
        self._commands[command_name] = command

    def execute(self, command_name, parameters):
        "Execute any registered commands"
        if command_name in self._commands.keys():
            self._commands[command_name].execute(parameters)
        else:
            print(f"Command [{command_name}] not recognised")

    def checkPhraseCommand(self,parameters):
        "Check if the parameters can make up to a phrase only parameter"
        s=parameters
        s = sub(r"(_|-)+", " ", s).title().replace(" ", "")
        s=''.join([s[0].lower(), s[1:]])
        if s in self._commands:
            return s
        else:
            return False
    def getCommands(self):
        return self._commands;
    def showCommands(self):
        for i in self._commands:
            print(i)
