import nltk

# Download NLTK resources (you only need to do this once)
#nltk.download('punkt')
#nltk.download('stopwords')
#nltk.download('maxent_ne_chunker')
#nltk.download('averaged_perceptron_tagger')
#nltk.download('words')

from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk import ne_chunk
class LanguageProcessor:
	def __init__(self):
		"New Language Processor"


	def processSentence(self,sentence):
		# Tokenize the sentence
		tokens = word_tokenize(sentence)

		# Remove stopwords (common words like 'the', 'in', 'is')
		stop_words = set(stopwords.words('english'))
		filtered_tokens = [word for word in tokens if word.lower() not in stop_words]

		# Perform Named Entity Recognition (NER)
		ner_result = ne_chunk(nltk.pos_tag(filtered_tokens))

		
		# Extract named entities from the NER result
		named_entities = [item for item in ner_result if isinstance(item, nltk.Tree)]
		
		return tokens, filtered_tokens, named_entities
