'''
Project: Open and Flexible Voice Project
Author: Subein Byanjankar, Msc ITM, Analyst Programmer, Nepal
Project-Title: PersonalDesktopAssistant
FileName: main.pyw
'''
from engine.initialize import CommandManager as cm

cuckoo=cm()
cuckoo.registerAICommands('training.json')
cuckoo.showCommands()
#msg="Could you tell me who is Iron Maiden"
#cuckoo.handleCommandAI(msg)

#main caller
while(True):
    x=cuckoo.listening()
    if x==True:
        pass
    else:
        y=cuckoo.listenCommand()
        if y==False:
            cuckoo.handleCommand("failed Pardon me, I could not understand")
        try:
            cuckoo.handleCommandAI(y)
        except Exception as e:
            print(e)


